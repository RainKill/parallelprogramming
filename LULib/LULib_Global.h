#pragma once

#ifndef LULib_Global_H
#define LULib_Global_H

#if defined(LULIB_EXPORT) // inside DLL
	#define LULIB   __declspec(dllexport)
#else // outside DLL
	#define LULIB   __declspec(dllimport)
#endif

#endif // !LULib_Global_H
