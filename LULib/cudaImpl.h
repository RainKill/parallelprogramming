#pragma once

#ifndef cudaImpl_H
#define cudaImpl_H

#include "LUDecomposer.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <assert.h>

class cudaImpl
{
public:
	cudaImpl();
	~cudaImpl();

	std::pair<matrix, matrix> decompose(const matrix& m);

private:

	int blockSize;

	template<typename T>
	class scoped_cuda_array
	{
	public:
		scoped_cuda_array()
		{
			_data = nullptr;
		}

		scoped_cuda_array(size_t size)
		{
			_size = size;
			cudaError_t error = cudaMalloc<T>(&_data, sizeof(T) * size);
			if (error != cudaSuccess)
			{
				_data = nullptr;
			}
		}
		scoped_cuda_array(T* data, size_t size)
		{
			_size = size;
			_data = data;
		}

		~scoped_cuda_array()
		{
			if (_data)
			{
				cudaFree(_data);
			}
		}

		size_t lenght() const
		{
			return _size;
		}

		T* get()
		{
			return _data;
		}

		bool copyFromHost(T* dataOnHost, size_t size)
		{
			assert(_data && size <= _size);

			cudaError_t error = cudaMemcpy(_data, dataOnHost, sizeof(T) * size, cudaMemcpyHostToDevice);

			return error == cudaSuccess;
		}
		bool copyToHost(T* dataOnHost, size_t size)
		{
			assert(_data && size <= _size);

			cudaError_t error = cudaMemcpy(dataOnHost, _data, sizeof(T) * size, cudaMemcpyDeviceToHost);

			return error == cudaSuccess;
		}

		bool isEmpty() const
		{
			return _data == nullptr;
		}

		explicit operator bool() const
		{
			return _data != nullptr;
		}
	private:
		scoped_cuda_array(const scoped_cuda_array& other) = delete;
		scoped_cuda_array& operator=(const scoped_cuda_array& other) = delete;

		T* _data;

		size_t _size;
	};
};

#endif // !cudaImpl_H


