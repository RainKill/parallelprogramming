#pragma once

#ifndef sequentialImpl_H
#define sequentialImpl_H

#include "LUDecomposer.h"

class sequentialImpl
{
public:
	sequentialImpl();
	~sequentialImpl();

	std::pair<matrix, matrix> decompose(const matrix& m);
};


#endif // !sequentialImpl_H
