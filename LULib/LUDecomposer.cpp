#include "LUDecomposer.h"

#include "sequentialImpl.h"
#include "openMPImpl.h"
#include "cudaImpl.h"

LUDecomposer::LUDecomposer()
{
	_sqImpl = new sequentialImpl();
	_oMPIpml = new openMPImpl();
	_cudaImpl = new cudaImpl();
}

LUDecomposer::~LUDecomposer()
{
	delete _sqImpl;
	delete _oMPIpml;
	delete _cudaImpl;
}

std::pair<matrix, matrix> LUDecomposer::decompose(ApproachType type, const matrix& m) noexcept
{
	switch (type)
	{
		case LUDecomposer::ApproachType::Sequential:
			return _sqImpl->decompose(m);
			break;

		case LUDecomposer::ApproachType::OpenMP:
			return _oMPIpml->decompose(m);
			break;

		case LUDecomposer::ApproachType::CUDA:
			return _cudaImpl->decompose(m);
			break;

		default:
			break;
	}

	return std::make_pair(matrix(), matrix());
}
