#pragma once

#ifndef flatMatrix_H
#define flatMatrix_H

#include "matrix.h"

class flatMatrix
{
public:
	flatMatrix();
	flatMatrix(int rowCount, int columnCount, double defaulValue = 0.0);
	flatMatrix(int n, double defaulValue = 0.0);
	flatMatrix(const flatMatrix& other);
	flatMatrix(const matrix& m);

	~flatMatrix();

	int rowCount() const;
	int columnCount() const;
	bool isEmpty() const;

	double* get();

	matrix toMatrix();

	double& operator[](int i);
	flatMatrix& operator=(const flatMatrix& other);
	flatMatrix& operator=(const matrix& m);

	friend std::ostream& operator<< (std::ostream& stream, const flatMatrix& fM);

private:
	void defaultInit();
	void initWithParams(int rowCount, int columnCount, double defaulValue);
	void clearData();

	int _rowCount;
	int _columnCount;

	double* _data;
};

#endif // !flatMatrix_H
