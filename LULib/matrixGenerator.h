#pragma once

#ifndef matrixGenerator_H
#define matrixGenerator_H

#include "matrix.h"

extern "C"
{
	class LULIB matrixGenerator
	{
	public:
		matrixGenerator();
		~matrixGenerator();

		matrix generate(int size);
		matrix generateIdentityMatrix(int size);

	private:
		matrix parallelGenerate(int size);
	};
}

#endif // !matrixGenerator_H
