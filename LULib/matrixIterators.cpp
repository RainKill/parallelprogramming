#include "matrix.h"
#include <assert.h>

matrix::itemIterator & matrix::itemIterator::operator++()
{
	_currentIndex++;

	return *this;
}

matrix::itemIterator matrix::itemIterator::operator++(int)
{
	itemIterator res(*this);

	++(*this);

	return res;
}

double & matrix::itemIterator::operator*()
{
	return value();
}

bool matrix::itemIterator::operator==(const itemIterator & other)
{
	return _items == other._items && _currentIndex == other._currentIndex;
}

bool matrix::itemIterator::operator!=(const itemIterator & other)
{
	return _items == other._items && _currentIndex != other._currentIndex;
}

int matrix::itemIterator::currentIndex()
{
	return _currentIndex;
}

double & matrix::itemIterator::value()
{
	return  _items[_currentIndex];
}

matrix::itemIterator & matrix::itemIterator::operator=(const itemIterator & other)
{
	_currentIndex = other._currentIndex;
	_items = other._items;

	return *this;
}

matrix::itemIterator & matrix::itemIterator::operator=(double value)
{
	assert(_currentIndex > -1 && _currentIndex < _maxIndex);

	_items[_currentIndex] = value;

	return *this;
}

matrix::itemIterator::itemIterator(int currentIndex, int maxIndex, double * items)
{
	_currentIndex = currentIndex;
	_maxIndex = maxIndex;
	_items = items;
}

matrix::itemIterator::itemIterator(const itemIterator & other)
{
	_currentIndex = other._currentIndex;
	_items = other._items;
}

matrix::itemIterator matrix::rowIterator::begin()
{
	return itemIterator(0, _parent->_columnCount, _parent->_data[_currentRow]);
}

matrix::itemIterator matrix::rowIterator::end()
{
	return itemIterator(_parent->_columnCount, _parent->_columnCount, _parent->_data[_currentRow]);
}

matrix::rowIterator & matrix::rowIterator::operator++()
{
	_currentRow++;

	return *this;
}

matrix::rowIterator matrix::rowIterator::operator++(int)
{
	rowIterator res(*this);

	++(*this);

	return res;
}

int matrix::rowIterator::currentRow()
{
	return _currentRow;
}

matrix::itemIterator matrix::rowIterator::operator[](int index)
{
	assert(index > -1 && index < _parent->_columnCount);
	return itemIterator(index, _parent->_columnCount, _parent->_data[_currentRow]);
}

bool matrix::rowIterator::operator==(const rowIterator & other)
{
	return _parent == other._parent && _currentRow == other._currentRow;
}

bool matrix::rowIterator::operator!=(const rowIterator & other)
{
	return _parent == other._parent && _currentRow != other._currentRow;
}

matrix::rowIterator & matrix::rowIterator::operator=(const rowIterator & other)
{
	_currentRow = other._currentRow;
	_parent = other._parent;

	return *this;
}

matrix::rowIterator::rowIterator(int currentRow, matrix * parent)
{
	_currentRow = currentRow;
	_parent = parent;
}

matrix::rowIterator::rowIterator(const rowIterator & other)
{
	_currentRow = other._currentRow;
	_parent = other._parent;
}

matrix::constItemIterator::constItemIterator(const constItemIterator & other)
{
	_currentIndex = other._currentIndex;
	_items = other._items;
}

matrix::constItemIterator::~constItemIterator()
{
}

int matrix::constItemIterator::currentIndex()
{
	return _currentIndex;
}

double matrix::constItemIterator::value()
{
	return _items[_currentIndex];
}

matrix::constItemIterator & matrix::constItemIterator::operator++()
{
	_currentIndex++;

	return *this;
}

matrix::constItemIterator matrix::constItemIterator::operator++(int)
{
	constItemIterator res(*this);

	++(*this);

	return res;
}

double matrix::constItemIterator::operator*()
{
	return value();
}

bool matrix::constItemIterator::operator==(const constItemIterator & other)
{
	return _items == other._items && _currentIndex == other._currentIndex;
}

bool matrix::constItemIterator::operator!=(const constItemIterator & other)
{
	return _items == other._items && _currentIndex != other._currentIndex;
}

matrix::constItemIterator & matrix::constItemIterator::operator=(const constItemIterator & other)
{
	_currentIndex = other._currentIndex;
	_items = other._items;

	return *this;
}

matrix::constItemIterator::constItemIterator(int currentIndex, int maxIndex, const double * items)
{
	_currentIndex = currentIndex;
	_maxIndex = maxIndex;
	_items = items;
}

matrix::constRowIterator::constRowIterator(const constRowIterator & other)
{
	_currentRow = other._currentRow;
	_parent = other._parent;
}

matrix::constItemIterator matrix::constRowIterator::begin()
{
	return constItemIterator(0, _parent->_columnCount, _parent->_data[_currentRow]);;
}

matrix::constItemIterator matrix::constRowIterator::end()
{
	return constItemIterator(_parent->_columnCount, _parent->_columnCount, _parent->_data[_currentRow]);;
}

int matrix::constRowIterator::currentRow()
{
	return _currentRow;
}

matrix::constRowIterator & matrix::constRowIterator::operator++()
{
	_currentRow++;

	return *this;
}

matrix::constRowIterator matrix::constRowIterator::operator++(int)
{
	constRowIterator res(*this);

	++(*this);

	return res;
}

matrix::constItemIterator matrix::constRowIterator::operator[](int index)
{
	assert(index > -1 && index < _parent->_columnCount);
	return constItemIterator(index, _parent->_columnCount, _parent->_data[_currentRow]);
}

bool matrix::constRowIterator::operator==(const constRowIterator & other)
{
	return _parent == other._parent && _currentRow == other._currentRow;
}

bool matrix::constRowIterator::operator!=(const constRowIterator & other)
{
	return _parent == other._parent && _currentRow != other._currentRow;
}

matrix::constRowIterator & matrix::constRowIterator::operator=(const constRowIterator & other)
{
	_currentRow = other._currentRow;
	_parent = other._parent;

	return *this;
}

matrix::constRowIterator::constRowIterator(int currentRow, const matrix * parent)
{
	_currentRow = currentRow;
	_parent = parent;
}
