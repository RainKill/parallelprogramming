#pragma once

#ifndef Matrix_H
#define Matrix_H

#include <ostream>

#include "LULib_Global.h"

class LULIB matrix
{
public:
	class LULIB itemIterator;
	class LULIB rowIterator;
	class LULIB constItemIterator;
	class LULIB constRowIterator;

#pragma region iterators
	class itemIterator
	{
		friend class rowIterator;
	public:
		itemIterator(const itemIterator& other);

		int currentIndex();
		double& value();

		itemIterator& operator++();
		itemIterator operator++(int);
		double& operator*();
		bool operator==(const itemIterator& other);
		bool operator!=(const itemIterator& other);

		itemIterator& operator=(const itemIterator& other);
		itemIterator& operator=(double value);
	private:
		itemIterator(int currentIndex, int maxIndex, double* items);

		int _currentIndex;
		int _maxIndex;
		double *_items;
	};

	class rowIterator
	{
		friend class matrix;
	public:
		rowIterator(const rowIterator& other);

		itemIterator begin();
		itemIterator end();

		int currentRow();

		rowIterator& operator++();
		rowIterator operator++(int);
		itemIterator operator[](int index);
		bool operator==(const rowIterator& other);
		bool operator!=(const rowIterator& other);

		rowIterator& operator=(const rowIterator& other);
	private:
		rowIterator(int currentRow, matrix *parent);


		int _currentRow;

		matrix *_parent;
	};
#pragma endregion

	class constItemIterator
	{
		friend class constRowIterator;
	public:
		constItemIterator(const constItemIterator& other);
		~constItemIterator();

		int currentIndex();
		double value();

		constItemIterator& operator++();
		constItemIterator operator++(int);
		double operator*();
		bool operator==(const constItemIterator& other);
		bool operator!=(const constItemIterator& other);

		constItemIterator& operator=(const constItemIterator& other);

	private:
		constItemIterator(int currentIndex, int maxIndex, const double* items);

		int _currentIndex;
		int _maxIndex;
		const double *_items;
	};

	class constRowIterator
	{
		friend class matrix;
	public:
		constRowIterator(const constRowIterator& other);

		constItemIterator begin();
		constItemIterator end();

		int currentRow();

		constRowIterator& operator++();
		constRowIterator operator++(int);
		constItemIterator operator[](int index);
		bool operator==(const constRowIterator& other);
		bool operator!=(const constRowIterator& other);

		constRowIterator& operator=(const constRowIterator& other);
	private:
		constRowIterator(int currentRow, const matrix *parent);

		int _currentRow;

		const matrix *_parent;
	};

	matrix();
	matrix(int rowCount, int columnCount, double defaultValue = 0.0);
	matrix(int n, double defaultValue = 0.0);
	matrix(const matrix& other);
	~matrix();

	rowIterator begin();
	constRowIterator begin() const;

	rowIterator end();
	constRowIterator end() const;

	int rowCount() const;
	int columnCount() const;
	bool isEmpty() const;

	rowIterator operator[](int index);
	constRowIterator operator[](int index) const;
	matrix& operator=(const matrix& other);
	bool operator==(const matrix& other) const;
	bool operator!=(const matrix& other) const;

	friend LULIB matrix operator*(const matrix& lhs, const matrix& rhs);
	friend LULIB matrix operator-(const matrix& lhs, const matrix& rhs);
	friend LULIB std::ostream& operator<< (std::ostream& stream, const matrix& m);

private:
	static const double _epsilon;
	
	void defaultInit();
	void initWithSize(int rowCount, int columnCount);
	void fillWith(double value);
	void clearData();

	int _rowCount;
	int _columnCount;

	double **_data;
};

#endif // !Matrix_H


