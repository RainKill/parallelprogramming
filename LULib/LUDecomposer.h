#pragma once

#ifndef LU_H
#define LU_H

#include "LULib_Global.h"
#include "matrix.h"

class sequentialImpl;
class openMPImpl;
class cudaImpl;

extern "C"
{

	class LULIB LUDecomposer
	{
	public:
		enum class ApproachType : int
		{
			Sequential = 0,
			OpenMP = 1,
			CUDA = 2
		};

		LUDecomposer();
		~LUDecomposer();

		std::pair<matrix, matrix> decompose(ApproachType type, const matrix& m) noexcept;

	private:
		sequentialImpl* _sqImpl;
		openMPImpl* _oMPIpml;
		cudaImpl* _cudaImpl;
	};

}
#endif // !LU_H

