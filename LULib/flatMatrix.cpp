#include "flatMatrix.h"

#include <assert.h>

flatMatrix::flatMatrix()
{
	defaultInit();
}

flatMatrix::flatMatrix(int rowCount, int columnCount, double defaulValue)
{
	initWithParams(rowCount, columnCount, defaulValue);
}

flatMatrix::flatMatrix(int n, double defaulValue)
{
	initWithParams(n, n, defaulValue);
}

flatMatrix::flatMatrix(const flatMatrix & other)
{
	defaultInit();

	*this = other;
}

flatMatrix::flatMatrix(const matrix & m)
{
	defaultInit();

	*this = m;
}


flatMatrix::~flatMatrix()
{
	clearData();
}

int flatMatrix::rowCount() const
{
	return 0;
}

int flatMatrix::columnCount() const
{
	return 0;
}

bool flatMatrix::isEmpty() const
{
	return _rowCount <= 0 && _columnCount <= 0;
}

double * flatMatrix::get()
{
	return _data;
}

matrix flatMatrix::toMatrix()
{
	matrix m(_rowCount, _columnCount);

	for (int i = 0; i < _rowCount; ++i)
	{
		for (int j = 0; j < _columnCount; ++j)
		{
			m[i][j] = (*this)[j + (i * _columnCount)];
		}
	}

	return m;
}

double & flatMatrix::operator[](int i)
{
	assert(i > -1 && i < (_rowCount * _columnCount));
	return _data[i];
}

flatMatrix & flatMatrix::operator=(const flatMatrix & other)
{
	_rowCount = other._rowCount;
	_columnCount = other._columnCount;

	clearData();

	_data = new double[_rowCount * _columnCount];
	std::memcpy(_data, other._data, sizeof(double) * (_rowCount * _columnCount));

	return *this;
}

flatMatrix & flatMatrix::operator=(const matrix & m)
{
	_rowCount = m.rowCount();
	_columnCount = m.columnCount();

	clearData();

	_data = new double[_rowCount * _columnCount];

	for (int i = 0; i < _rowCount; ++i)
	{
		for (int j = 0; j < _columnCount; ++j)
		{
			_data[j + (i * _columnCount)] = m[i][j].value();
		}
	}

	return *this;
}

void flatMatrix::defaultInit()
{
	_data = nullptr;
	_rowCount = 0;
	_columnCount = 0;
}

void flatMatrix::initWithParams(int rowCount, int columnCount, double defaulValue)
{
	_rowCount = rowCount;
	_columnCount = columnCount;

	_data = new double[_rowCount * _columnCount];
	std::memset(_data, defaulValue, sizeof(double) * (_rowCount * _columnCount));
}

void flatMatrix::clearData()
{
	delete[] _data;
	_data = nullptr;
}

std::ostream & operator<<(std::ostream & stream, const flatMatrix & fM)
{
	if (fM.isEmpty())
	{
		stream << "empty matrix" << std::endl;
	}
	else
	{
		for (int i = 0; i < fM._rowCount; ++i)
		{
			for (int j = 0; j < fM._columnCount; ++j)
			{
				stream << fM._data[j + (i * fM._columnCount)] << " ";
			}
			stream << std::endl;
		}
	}

	return stream;
}
