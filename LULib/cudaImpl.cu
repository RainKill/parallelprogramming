#include "cudaImpl.h"

#include "flatMatrix.h"

__global__ void doStep(double* L, double* U, int k, int n, int blockSize)
{
	int j = (blockSize * blockIdx.x) + threadIdx.x;
	if (j >= k + 1 && j < n)
	{
		double factor = U[(j * n) + k] / U[(k * n) + k];
		int i = (blockSize * blockIdx.y) + threadIdx.y;
		if (i < n)
		{
			U[(j * n) + i] = U[(j * n) + i] - (U[(k * n) + i] * factor);
		}

		if (L[(j * n) + k] != factor)
		{
			L[(j * n) + k] = factor;
		}
	}
}

cudaImpl::cudaImpl()
{
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	blockSize = sqrt(prop.maxThreadsPerBlock);
}


cudaImpl::~cudaImpl()
{
}

std::pair<matrix, matrix> cudaImpl::decompose(const matrix& m)
{
	int n = m.rowCount();
	
	flatMatrix flatL(n);
	for (int i = 0; i < n; ++i)
	{
		flatL[i + (n * i)] = 1;
	}
	flatMatrix flatU(m);

	scoped_cuda_array<double> deviceL(n * n);
	scoped_cuda_array<double> deviceU(n * n);

	bool lCopy = deviceL.copyFromHost(flatL.get(), n * n);
	bool uCopy = deviceU.copyFromHost(flatU.get(), n * n);

	if (!lCopy && !uCopy)
	{
		return std::pair<matrix, matrix>();
	}

	dim3 threadsPerBlock(blockSize, blockSize, 0);

	double coverageRatio = static_cast<double>(n) / blockSize;
	double tmpBlockCount = 0.0;
	volatile int blockCount = std::trunc(coverageRatio);

	if (std::modf(coverageRatio, &tmpBlockCount) != 0.0)
	{
		blockCount += 1;
	}

	dim3 blocksPerGrid(blockCount, blockCount, 0);

	for (int i = 0; i < n - 1; ++i)
	{
		doStep<<<blocksPerGrid, threadsPerBlock>>> (deviceL.get(), deviceU.get(), i, n, blockSize);
	}

	lCopy = deviceL.copyToHost(flatL.get(), n * n);
	uCopy = deviceU.copyToHost(flatU.get(), n * n);

	if (!lCopy && !uCopy)
	{
		return std::pair<matrix, matrix>();
	}

	return std::make_pair(flatL.toMatrix(), flatU.toMatrix());
}
