#include "openMPImpl.h"

#include <omp.h>


openMPImpl::openMPImpl()
{
}


openMPImpl::~openMPImpl()
{
}

std::pair<matrix, matrix> openMPImpl::decompose(const matrix& m)
{
	const int n = m.rowCount();
	matrix U = m;
	matrix L(n);

	for (int k = 0; k < n - 1; k++) {
#pragma omp parallel for
		for (int j = k + 1; j < n; j++) {
			double factor = U[j][k].value() / U[k][k].value();
			for (int i = 0; i < n; i++) {
				U[j][i] = U[j][i].value() - (U[k][i].value() * factor);
			}
			L[j][k] = factor;
		}
	}

	for (int i = 0; i < n; i++) {
		L[i][i] = 1;
	}

	return std::make_pair(L, U);
}
