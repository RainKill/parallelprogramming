#include "matrix.h"

#include <memory>
#include <assert.h>
#include <math.h>
#include <numeric>

#define ORDER_OF_MAGNITUDE 1.E5

const double matrix::_epsilon = std::numeric_limits<double>::epsilon() * ORDER_OF_MAGNITUDE;

matrix::matrix()
{
	defaultInit();
}

matrix::matrix(int rowCount, int columnCount, double defaultValue)
{
	initWithSize(rowCount, columnCount);

	fillWith(defaultValue);
}

matrix::matrix(int n, double defaultValue)
{
	initWithSize(n, n);

	fillWith(defaultValue);
}

matrix::matrix(const matrix & other)
{
	defaultInit();

	*this = other;
}


matrix::~matrix()
{
	clearData();
}

matrix::rowIterator matrix::begin()
{
	return rowIterator(0, this);
}

matrix::constRowIterator matrix::begin() const
{
	return constRowIterator(0, this);
}

matrix::rowIterator matrix::end()
{
	return rowIterator(_rowCount, this);
}

matrix::constRowIterator matrix::end() const
{
	return constRowIterator(_rowCount, this);
}

int matrix::rowCount() const
{
	return _rowCount;
}

int matrix::columnCount() const
{
	return _columnCount;
}

bool matrix::isEmpty() const
{
	return _rowCount <= 0 && _columnCount <= 0;
}

matrix::rowIterator matrix::operator[](int index)
{
	assert(index > -1 && index < _rowCount);
	return rowIterator(index, this);
}

matrix::constRowIterator matrix::operator[](int index) const
{
	assert(index > -1 && index < _rowCount);
	return constRowIterator(index, this);
}

matrix & matrix::operator=(const matrix & other)
{
	clearData();

	_rowCount = other._rowCount;
	_columnCount = other._columnCount;

	if (!isEmpty())
	{
		_data = new double*[_rowCount];
		for (int i = 0; i < _rowCount; ++i)
		{
			_data[i] = new double[_columnCount];
			std::memcpy(_data[i], other._data[i], _columnCount * sizeof(double));
		}
	}

	return *this;
}

bool matrix::operator==(const matrix & other) const
{
	if (_rowCount == other._rowCount && _columnCount == other._columnCount)
	{
		for (int i = 0; i < _rowCount; ++i)
		{
			for (int j = 0; j < _columnCount; ++j)
			{
				if (fabs(_data[i][j] - other._data[i][j]) > _epsilon)
				{
					return false;
				}
			}
		}

		return true;
	}

	return false;
}

bool matrix::operator!=(const matrix & other) const
{
	return !this->operator==(other);
}

void matrix::defaultInit()
{
	_data = nullptr;
	_rowCount = 0;
	_columnCount = 0;
}

void matrix::initWithSize(int rowCount, int columnCount)
{
	_rowCount = rowCount;
	_columnCount = columnCount;

	_data = new double*[rowCount];
	for (int i = 0; i < rowCount; ++i)
	{
		_data[i] = new double[columnCount];
	}
}

void matrix::fillWith(double value)
{
	for (int i = 0; i < _rowCount; ++i)
	{
		for (int j = 0; j < _columnCount; ++j)
		{
			_data[i][j] = value;
		}
	}
}

void matrix::clearData()
{
	for (int i = 0; i < _rowCount; ++i)
	{
		delete[] _data[i];
	}

	delete[] _data;

	_data = nullptr;
}

matrix operator*(const matrix & lhs, const matrix & rhs)
{
	assert(lhs._columnCount == rhs._rowCount);

	matrix m(lhs._rowCount, rhs._columnCount);

	for (int i = 0; i < lhs._rowCount; ++i)
	{
		for (int j = 0; j < rhs._columnCount; ++j)
		{
			for (int k = 0; k < lhs._columnCount; ++k)
			{
				m._data[i][j] += lhs._data[i][k] * rhs._data[k][j];
			}
		}
	}

	return m;
}

matrix operator-(const matrix & lhs, const matrix & rhs)
{
	assert(lhs._columnCount == rhs._columnCount && lhs._rowCount == rhs._rowCount);

	matrix m(lhs._columnCount, lhs._rowCount);

	for (int i = 0; i < lhs._rowCount; ++i)
	{
		for (int j = 0; j < lhs._columnCount; ++j)
		{
			m._data[i][j] = lhs._data[i][j] - rhs._data[i][j];
		}
	}

	return m;
}

std::ostream & operator<<(std::ostream & stream, const matrix & m)
{
	if (m.isEmpty())
	{
		stream << "empty matrix" << std::endl;
	}
	else
	{
		for (int i = 0; i < m._rowCount; ++i)
		{
			for (int j = 0; j < m._columnCount; ++j)
			{
				stream << m._data[i][j] << " ";
			}
			stream << std::endl;
		}
	}

	return stream;
}
