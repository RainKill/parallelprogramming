#pragma once

#ifndef openMPImpl_H
#define openMPImpl_H

#include "LUDecomposer.h"

class openMPImpl
{
public:
	openMPImpl();
	~openMPImpl();

	std::pair<matrix, matrix> decompose(const matrix& m);
};


#endif // !openMPImpl_H
