#include "matrixGenerator.h"

#include <chrono>
#include <random>

#include <omp.h>

matrixGenerator::matrixGenerator()
{

}


matrixGenerator::~matrixGenerator()
{
}

matrix matrixGenerator::generate(int size)
{
	return parallelGenerate(size);
}

matrix matrixGenerator::generateIdentityMatrix(int size)
{
	matrix m(size);

	for (int i = 0; i < size; ++i)
	{
		m[i][i] = 1;
	}

	return m;
}

matrix matrixGenerator::parallelGenerate(int size)
{
	matrix m(size);

	std::default_random_engine generator(std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count()));
	std::uniform_int_distribution<int> distribution(std::uniform_int_distribution<int>(-100, 100));

	#pragma omp parallel for shared(m, generator, distribution)
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			m[i][j] = distribution(generator);
		}
	}

	return m;
}
