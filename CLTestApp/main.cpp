#include <iostream>

#include "tester.h"

int main(int argc, char *argv[])
{
	tester t;

	std::string separator = std::string(80, '=');

	//std::cout << separator << std::endl;

	//std::cout << t.testCase1(LUDecomposer::ApproachType::Sequential) << std::endl;
	//std::cout << t.testCase1(LUDecomposer::ApproachType::OpenMP) << std::endl;
	//std::cout << t.testCase1(LUDecomposer::ApproachType::CUDA) << std::endl;

	//std::cout << separator << std::endl;

	//std::cout << t.testCase2(LUDecomposer::ApproachType::Sequential) << std::endl;
	//std::cout << t.testCase2(LUDecomposer::ApproachType::OpenMP) << std::endl;
	//std::cout << t.testCase2(LUDecomposer::ApproachType::CUDA) << std::endl;

	//std::cout << separator << std::endl;

	//std::cout << t.testCase3(LUDecomposer::ApproachType::Sequential) << std::endl;
	//std::cout << t.testCase3(LUDecomposer::ApproachType::OpenMP) << std::endl;
	//std::cout << t.testCase3(LUDecomposer::ApproachType::CUDA) << std::endl;

	//std::cout << separator << std::endl;

	//std::cout << t.testCase4(LUDecomposer::ApproachType::Sequential) << std::endl;
	//std::cout << t.testCase4(LUDecomposer::ApproachType::OpenMP) << std::endl;
	//std::cout << t.testCase4(LUDecomposer::ApproachType::CUDA) << std::endl;

	std::cout << separator << std::endl;

	for (int i = 2; i < pow(2,12); i = i * 2)
	{
		std::cout << t.testRandom(LUDecomposer::ApproachType::Sequential, i) << std::endl;
		std::cout << t.testRandom(LUDecomposer::ApproachType::OpenMP, i) << std::endl;
		std::cout << t.testRandom(LUDecomposer::ApproachType::CUDA, i) << std::endl;
		std::cout << separator << std::endl;
	}

	return 0;
}