#pragma once

#ifndef tester_h
#define tester_h

#include <string>

#include "LUDecomposer.h"

class tester
{
public:
	tester();
	~tester();

	std::string testCase1(LUDecomposer::ApproachType type);
	std::string testCase2(LUDecomposer::ApproachType type);
	std::string testCase3(LUDecomposer::ApproachType type);
	std::string testCase4(LUDecomposer::ApproachType type);
	std::string testRandom(LUDecomposer::ApproachType type, int size);

private:
	std::string enumeratuinToString(LUDecomposer::ApproachType type);
	std::string boolToString(bool res);
	std::string testResultToString(const std::string& testName, int mRowCount, int mColumnCount, LUDecomposer::ApproachType type, bool passed, double eps, double elapsedTime);

	std::string doTest(const std::string& testName, LUDecomposer::ApproachType type, const matrix& a);

	bool isResultOk(double maxEps);
	double calcEpsilon(const matrix& a, const matrix& L, const matrix& U);

	double findMaxAbsElement(const matrix& m);

	LUDecomposer _decomposer;

	matrix _lastMatrix;
};

#endif // !tester_h
