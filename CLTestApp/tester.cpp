#include "tester.h"

#include <chrono>
#include <sstream>

#include "matrixGenerator.h"

tester::tester()
{
}


tester::~tester()
{
}

std::string tester::testCase1(LUDecomposer::ApproachType type)
{
	matrix a(3);

	a[0][0] = 1.0; a[0][1] = 4.0; a[0][2] = 7.0;
	a[1][0] = 2.0; a[1][1] = 5.0; a[1][2] = 8.0;
	a[2][0] = 3.0; a[2][1] = 6.0; a[2][2] = 10.0;

	matrix L(3);

	L[0][0] = 1.0; L[0][1] = 0.0; L[0][2] = 0.0;
	L[1][0] = 2.0; L[1][1] = 1.0; L[1][2] = 0.0;
	L[2][0] = 3.0; L[2][1] = 2.0; L[2][2] = 1.0;

	matrix U(3);

	U[0][0] = 1.0; U[0][1] = 4.0; U[0][2] = 7.0;
	U[1][0] = 0.0; U[1][1] = -3.0; U[1][2] = -6.0;
	U[2][0] = 0.0; U[2][1] = 0.0; U[2][2] = 1.0;

	return doTest(__FUNCTION__, type, a);
}

std::string tester::testCase2(LUDecomposer::ApproachType type)
{
	matrix a(3);

	a[0][0] = 10.0; a[0][1] = -7.0; a[0][2] = 0.0;
	a[1][0] = -3.0; a[1][1] = 6.0; a[1][2] = 2.0;
	a[2][0] = 5.0; a[2][1] = -1.0; a[2][2] = 5.0;

	matrix L(3);

	L[0][0] = 1.0; L[0][1] = 0.0; L[0][2] = 0.0;
	L[1][0] = -3.0 / 10.0; L[1][1] = 1.0; L[1][2] = 0.0;
	L[2][0] = 0.5; L[2][1] = 25.0 / 39.0; L[2][2] = 1.0;

	matrix U(3);

	U[0][0] = 10.0; U[0][1] = -7.0; U[0][2] = 0.0;
	U[1][0] = 0.0; U[1][1] = 39.0 / 10.0; U[1][2] = 2;
	U[2][0] = 0.0; U[2][1] = 0.0; U[2][2] = 145.0 / 39.0;

	return doTest(__FUNCTION__, type, a);
}

std::string tester::testCase3(LUDecomposer::ApproachType type)
{
	matrix a(3);

	a[0][0] = 1.0; a[0][1] = 3.0; a[0][2] = 2.0;
	a[1][0] = 1.0; a[1][1] = 1.0; a[1][2] = 4.0;
	a[2][0] = 2.0; a[2][1] = 2.0; a[2][2] = 1.0;

	matrix L(3);

	L[0][0] = 1.0; L[0][1] = 0.0; L[0][2] = 0.0;
	L[1][0] = 1.0; L[1][1] = 1.0; L[1][2] = 0.0;
	L[2][0] = 2.0; L[2][1] = 2.0; L[2][2] = 1.0;

	matrix U(3);

	U[0][0] = 1.0; U[0][1] = 3.0; U[0][2] = 2.0;
	U[1][0] = 0.0; U[1][1] = -2.0; U[1][2] = 2.0;
	U[2][0] = 0.0; U[2][1] = 0.0; U[2][2] = -7.0;

	return doTest(__FUNCTION__, type, a);
}

std::string tester::testCase4(LUDecomposer::ApproachType type)
{
	matrix a(3);

	a[0][0] = -99.0; a[0][1] = 67.0; a[0][2] = -39.0;
	a[1][0] = -94.0; a[1][1] = -8.0; a[1][2] = -30.0;
	a[2][0] = -92.0; a[2][1] = -19.0; a[2][2] = 56.0;

	matrix L(3);

	L[0][0] = 1.0; L[0][1] = 0.0; L[0][2] = 0.0;
	L[1][0] = 94.0 / 99.0; L[1][1] = 1.0; L[1][2] = 0.0;
	L[2][0] = 92.0 / 99.0; L[2][1] = 1609.0 / 1418.0; L[2][2] = 1.0;

	matrix U(3);

	U[0][0] = -99.0; U[0][1] = 67.0; U[0][2] = -39.0;
	U[1][0] = 0.0; U[1][1] = -7090.0 / 99.0; U[1][2] = 232.0 / 33.0;
	U[2][0] = 0.0; U[2][1] = 0.0; U[2][2] = 59744.0 / 709.0;

	return doTest(__FUNCTION__, type, a);
}

std::string tester::testRandom(LUDecomposer::ApproachType type, int size)
{
	matrixGenerator generator;

	if (_lastMatrix.columnCount() != size)
	{
		_lastMatrix = generator.generate(size);
	}

	return doTest(__FUNCTION__, type, _lastMatrix);
}

std::string tester::enumeratuinToString(LUDecomposer::ApproachType type)
{
	switch (type)
	{
	case LUDecomposer::ApproachType::Sequential:
		return "Sequential";
		break;
	case LUDecomposer::ApproachType::OpenMP:
		return "OpenMP";
		break;
	case LUDecomposer::ApproachType::CUDA:
		return "CUDA";
		break;
	default:
		break;
	}
	return std::string();
}

std::string tester::boolToString(bool res)
{
	if (res)
	{
		return "passed";
	}

	return "failed";
}

std::string tester::testResultToString(const std::string& testName, int mRowCount, int mColumnCount, LUDecomposer::ApproachType type, bool passed, double eps, double elapsedTime)
{
	std::stringstream stream;

	stream << testName;
	stream << std::endl << "matrix size: " << mRowCount << "x" << mColumnCount;
	stream << " Approach type: " << enumeratuinToString(type);
	stream << std::endl << "result - " << boolToString(passed);
	stream << " (max epsilon = " << eps << ").";
	stream << '\t' << "Elapsed time: " << elapsedTime << " seconds";

	return stream.str();
}

std::string tester::doTest(const std::string & testName, LUDecomposer::ApproachType type, const matrix & a)
{
	std::chrono::high_resolution_clock::time_point t1, t2;

	t1 = std::chrono::high_resolution_clock::now();
	auto r = _decomposer.decompose(type, a);
	t2 = std::chrono::high_resolution_clock::now();

	double eps = calcEpsilon(a, r.first, r.second);

	bool res = isResultOk(eps);

	std::chrono::duration<double> dur = t2 - t1;
	double elapsedTime = dur.count();

	return testResultToString(testName, a.rowCount(), a.columnCount(), type, res, eps, elapsedTime);
}


bool tester::isResultOk(double maxEps)
{
	static const double orderOfMagnitude = 1.E-5;
	return maxEps < orderOfMagnitude;
}

double tester::calcEpsilon(const matrix & a, const matrix & L, const matrix & U)
{
	matrix a1 = L * U;

	return findMaxAbsElement(a - a1);
}

double tester::findMaxAbsElement(const matrix & m)
{
	double res = 0;

	for (auto rIt = m.begin(); rIt != m.end(); ++rIt)
	{
		for (auto cIt = rIt.begin(); cIt != rIt.end(); ++cIt)
		{
			if (fabs(cIt.value()) > res)
			{
				res = fabs(cIt.value());
			}
		}
	}

	return res;
}
